# wsESP8266-DHT11-Firebase-v2 #

*WiFi Weather Station Project*  update for the file  *wsESP8266-DHT11-Firebase.ino*  to fix the broken data push issue to Firebase after a recent library update.



### WiFi Weather Station Project ###

The WiFi Weather Station was created to use a Adafruit ESP-8266 Feather HUZZAH to connect to the internet via WiFi and measure the temperature and humidity in real time by using either a DHT-11 or DHT-22 temperature and humidity sensor to transmit the data via WiFi to Google Firebase, a real-time cloud database, and then display the data in a React.JS hosted website interface.



### Original WiFi Weather Station Project Repo ###

* URL:  https://github.com/DKMitt/wifi_ws



### What is this repository for? ###

* To fix a broken data push issues after library updates broke app.



### Working File List ###

* wsESP8266-DHT11-Firebase.ino - 1st version of code that broke
* wsESP8266-DHT11-Firebase-v2.ino  - New version (work in progress)



### Current Project Status ###

* Current Status:  Work in progress 06/28/2020